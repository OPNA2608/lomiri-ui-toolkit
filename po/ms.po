# Malay translation for lomiri-ui-toolkit
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-ui-toolkit package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-toolkit\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-11-05 10:05+0100\n"
"PO-Revision-Date: 2014-07-06 16:21+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Malay <ms@li.org>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2017-04-05 07:13+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: Lomiri/Components/1.2/TextInputPopover.qml:29
#: Lomiri/Components/1.3/TextInputPopover.qml:29
msgid "Select All"
msgstr "Pilih Semua"

#: Lomiri/Components/1.2/TextInputPopover.qml:36
#: Lomiri/Components/1.3/TextInputPopover.qml:36
msgid "Cut"
msgstr "Potong"

#: Lomiri/Components/1.2/TextInputPopover.qml:48
#: Lomiri/Components/1.3/TextInputPopover.qml:48
msgid "Copy"
msgstr "Salin"

#: Lomiri/Components/1.2/TextInputPopover.qml:57
#: Lomiri/Components/1.3/TextInputPopover.qml:57
msgid "Paste"
msgstr "Tampal"

#: Lomiri/Components/1.2/ToolbarItems.qml:143
#: Lomiri/Components/1.3/ToolbarItems.qml:143
msgid "Back"
msgstr "Kembali"

#: Lomiri/Components/ListItems/1.2/Empty.qml:398
#: Lomiri/Components/ListItems/1.3/Empty.qml:398
msgid "Delete"
msgstr "Hapus"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:51
msgid "No service/path specified"
msgstr "Tiada perkhidmatan/laluan dinyatakan"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:69
#, qt-format
msgid "Invalid bus type: %1."
msgstr "Jenis bas tidak sah: %1."

#. TRANSLATORS: Time based "this is happening/happened now"
#: Lomiri/Components/plugin/i18n.cpp:268
msgid "Now"
msgstr "Sekarang"

#: Lomiri/Components/plugin/i18n.cpp:275
#, qt-format
msgid "%1 minute ago"
msgid_plural "%1 minutes ago"
msgstr[0] "%1 minit yang lalu"
msgstr[1] "%1 minit yang lalu"

#: Lomiri/Components/plugin/i18n.cpp:277
#, qt-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minit"
msgstr[1] "%1 minit"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:284
msgid "h:mm ap"
msgstr "h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:287
msgid "HH:mm"
msgstr "HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:293
msgid "'Yesterday 'h:mm ap"
msgstr "'Semalam 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:296
msgid "'Yesterday 'HH:mm"
msgstr "'Semalam 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:302
msgid "'Tomorrow 'h:mm ap"
msgstr "'Esok 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:305
msgid "'Tomorrow 'HH:mm"
msgstr "'Esok 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:312
msgid "ddd' 'h:mm ap"
msgstr "ddd' 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:315
msgid "ddd' 'HH:mm"
msgstr "ddd' 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:322
msgid "ddd d MMM' 'h:mm ap"
msgstr "ddd d MMM' 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:325
msgid "ddd d MMM' 'HH:mm"
msgstr "ddd d MMM' 'HH:mm"

#: Lomiri/Components/plugin/privates/listitemdragarea.cpp:122
msgid ""
"ListView has no ViewItems.dragUpdated() signal handler implemented. No "
"dragging will be possible."
msgstr ""
"ListView tidak mempunyai pengendali isyarat ViewItems.dragUpdated() yang "
"dilaksanakan. Tiada penyeretan boleh dilakukan."

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:176
#, qt-format
msgid ""
"property \"%1\" of object %2 has type %3 and cannot be set to value \"%4\" "
"of type %5"
msgstr ""
"sifat \"%1\" bagi objek %2 mempunyai jenis %3 dan tidak boleh ditetapkan ke "
"nilai \"%4\" bagi jenis %5"

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:185
#, qt-format
msgid "property \"%1\" does not exist or is not writable for object %2"
msgstr "sifat \"%1\" tidak wujud atau tidak boleh ditulis untuk objek %2"

#: Lomiri/Components/plugin/ucalarm.cpp:41
#: Lomiri/Components/plugin/ucalarm.cpp:643
msgid "Alarm"
msgstr "Penggera"

#: Lomiri/Components/plugin/ucalarm.cpp:635
#: Lomiri/Components/plugin/ucalarm.cpp:667
msgid "Alarm has a pending operation."
msgstr "Penggera mempunyai operasi tertangguh."

#: Lomiri/Components/plugin/ucarguments.cpp:188
msgid "Usage: "
msgstr "Penggunaan: "

#: Lomiri/Components/plugin/ucarguments.cpp:209
msgid "Options:"
msgstr "Pilihan:"

#: Lomiri/Components/plugin/ucarguments.cpp:498
#, qt-format
msgid "%1 is expecting an additional argument: %2"
msgstr "%1 menjangkakan argumen tambahan: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:503
#, qt-format
msgid "%1 is expecting a value for argument: %2"
msgstr "%1 menjangkakan nilai untuk argumen: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:520
#, qt-format
msgid "%1 is expecting additional arguments: %2"
msgstr "%1 menjangkakan argumen tambahan: %2"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:145
msgid "consider overriding swipeEvent() slot!"
msgstr "anggap membatalkan slot swipeEvent()!"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:165
msgid "consider overriding rebound() slot!"
msgstr "anggap membatalkan slot rebound()!"

#: Lomiri/Components/plugin/ucmousefilters.cpp:1065
msgid "Ignoring AfterItem priority for InverseMouse filters."
msgstr "Mengabai keutamaan AfterItem untuk penapis InverseMouse."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:77
msgid "Changing connection parameters forbidden."
msgstr "Pengubahan parameter sambungan dilarang."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:160
#, qt-format
msgid ""
"Binding detected on property '%1' will be removed by the service updates."
msgstr ""
"Pengikatan dikesan pada sifat '%1' akan dibuang oleh kemaskini perkhidmatan."

#: Lomiri/Components/plugin/ucstatesaver.cpp:46
msgid "Warning: attachee must have an ID. State will not be saved."
msgstr "Amaran: lampiran mestilah mempunyai ID. Keadaan tidak akan disimpan."

#: Lomiri/Components/plugin/ucstatesaver.cpp:56
#, qt-format
msgid ""
"Warning: attachee's UUID is already registered, state won't be saved: %1"
msgstr "Amaran: lampiran UUID sudah berdaftar, keadaan tidak disimpan: %1"

#: Lomiri/Components/plugin/ucstatesaver.cpp:107
#, qt-format
msgid ""
"All the parents must have an id.\n"
"State saving disabled for %1, class %2"
msgstr ""
"Semua induk mesti mempunyai id.\n"
"Penyimpanan keadaan dilumpuhkan untuk %1, kelas %2"

#: Lomiri/Components/plugin/uctheme.cpp:208
#, qt-format
msgid "Theme not found: \"%1\""
msgstr "Tema tidak ditemui: \"%1\""

#: Lomiri/Components/plugin/uctheme.cpp:539
msgid "Not a Palette component."
msgstr "Tiada komponen Palet."

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:462
msgid "Dragging mode requires ListView"
msgstr "Mod penyeretan memerlukan ListView"

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:468
msgid ""
"Dragging is only supported when using a QAbstractItemModel, ListModel or "
"list."
msgstr ""
"Menyeretan hanya disokongg bila menggunakan QAbstractItemModel, ListModel "
"atau senarai."

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:78
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:78
msgid "Cancel"
msgstr "Batal"

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:88
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:88
msgid "Confirm"
msgstr "Sahkan"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:85
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:85
msgid "Close"
msgstr "Tutup"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:95
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:95
msgid "Done"
msgstr "Selesai"

#: Lomiri/Components/Themes/Ambiance/1.2/ProgressBarStyle.qml:51
#: Lomiri/Components/Themes/Ambiance/1.3/ProgressBarStyle.qml:50
msgid "In Progress"
msgstr "Dalam Proses"

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Release to refresh..."
msgstr "Lepaskan untuk segar semula..."

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Pull to refresh..."
msgstr "Tarik untuk segar semula..."

#~ msgid "Theme not found: "
#~ msgstr "Tema tidak ditemui: "
